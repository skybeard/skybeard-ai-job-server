import os
from flask import Flask, Response, jsonify, request
from flask_restful import Api
import socketio
import logging
sio = socketio.Client()
app = Flask(__name__)
app.logger.setLevel(logging.INFO)
logging.basicConfig(level=logging.INFO)
api = Api(app)
RESULTS = {}
RESPONSES = {}

@sio.on('result')
def handle_result(data):
    global RESULTS
    req_id = data.get('request',{}).get('id')
    new_word = data.get('response','')
    app.logger.debug('New Word: {}'.format(new_word))
    if req_id and new_word:
        RESULTS[req_id] = [False, data]
        if req_id not in RESPONSES:
            RESPONSES[req_id] = [new_word]
        else:
            RESPONSES[req_id].append(new_word)
        if '<end' in new_word or 'llama_print_timings:' in new_word or '[end of text]' in new_word:
            RESULTS[req_id][0] = True
    #else:
    #    RESULTS[req_id][1]['response'] += new_word
    
    app.logger.debug('Socket: ID: {} Response: {}'.format(req_id, RESULTS[req_id][1]['response']))   
        
@app.route("/send", methods=['POST'])
def send():
     data = request.get_json()
     sio.emit('request', data )
     app.logger.info("Emitting data with id {}".format(data['id']))
     return jsonify({"job_id": data['id']})

@app.route("/status")
def status():
    job_id = request.args.get('job_id', None)
    global RESULTS, RESPONSES
    job_status = False
    exists = False
    chunks = 0
    if job_id in RESULTS: 
        exists = True
        job_status = RESULTS[job_id][0]
        chunks = len(RESPONSES[job_id])

    return jsonify({'status': job_status, 'exists': exists, 'chunks': chunks })

@app.route("/results")
def get_results():
    job_id = request.args.get('job_id', None)
    delete = request.args.get('delete', True)
    if job_id in RESULTS:
        app.logger.info("Job {} found".format(job_id))
        if delete:
            status, result = RESULTS.pop(job_id)
            response = RESPONSES.pop(job_id)
        else:
            status, result = RESULTS[job_id]
            response = RESPONSES[job_id]

        result['job_status'] = status
        result['response_string'] = ''.join(response)
        return jsonify(result)
    app.logger.info("Job {} not found".format(job_id))
    return Response('', status=404)
          
if __name__ == '__main__':
    # open a client
    # try to connect
    try:
        sio.connect(os.environ['DALAI_HOST'], wait=True, wait_timeout=30)
    except Exception as e:
        app.logger.error(e)
        pass
    else:
        app.logger.info('Socket connection established')

    app.run(port=5005)
    #socketio.run(app, port=5005)