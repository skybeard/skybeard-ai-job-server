# Quickstart
Quickest way to build and run if you have Dalai container running on the same host:

```
docker build --tag skybeard-job-server .
docker run --network host -p 3000:3000 skybeard-job-server
```
